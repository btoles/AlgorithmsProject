package com.fourohfour;

import java.io.IOException;
import java.util.ArrayList;

public class Main {

    public static void main(String... arg) throws IOException {
        printToScreen();
    }


    public static void printOnlySneakyPathToScreen() throws IOException{

        long startTime= System.nanoTime();
        InputParser ip = new InputParser("n10d.txt");
        ip.parse();

        FloydWarshall floydwarshall = new FloydWarshall(ip.matrixSize, ip.original);
        System.out.println(floydwarshall.floydWarshall(ip.E));
        floydwarshall.generateFlowAmount(ip.F);
        floydwarshall.generateAllPairs(floydwarshall.flowAdjustedGraph);
        System.out.println();
        System.out.print("[");

        ArrayList<Integer> sneakyPath = floydwarshall.generatePath(ip.startNode, ip.endNode);

        for(Integer i: sneakyPath){
            i++;
            System.out.print(i.toString() + ',');
        }
        System.out.print("]\n");

        System.out.println("Sneaky Path Minimum --------------------------");
        System.out.println(floydwarshall.getPathMinimum(sneakyPath));
        System.out.println("Sneaky Path Maximum --------------------------");
        System.out.println(floydwarshall.getPathMaximum(sneakyPath));
        System.out.println("Sneaky Path Average --------------------------");
        System.out.println(floydwarshall.getPathAverage(sneakyPath));
        long endTime = System.nanoTime();
        System.out.println("Computation took " + ((endTime - startTime) / 1000000) + " milliseconds");

    }

    public static void printToScreen() throws IOException{

        long startTime= System.nanoTime();
        InputParser ip = new InputParser("n10d.txt");
        ip.parse();

        FloydWarshall floydwarshall = new FloydWarshall(ip.matrixSize, ip.original);
        System.out.println("Graph E --------------------------------------");
        floydwarshall.printMatrix(ip.E);
        System.out.println("Graph F --------------------------------------");
        floydwarshall.printMatrix(ip.F);
        System.out.println("Graph Shortest Path --------------------------");
        floydwarshall.printMatrix(floydwarshall.floydWarshall(ip.E));
        System.out.println("Graph Flow Edge Weights ----------------------");
        floydwarshall.printMatrix(floydwarshall.generateFlowAmount(ip.F));

        System.out.println();
        System.out.print("[");

        ArrayList<Integer> sneakyPath = floydwarshall.generatePath(ip.startNode, ip.endNode);

        for(Integer i: sneakyPath){
            i++;
            System.out.print(i.toString() + ',');
        }
        System.out.print("]\n");

        System.out.println("Sneaky Path Minimum --------------------------");
        System.out.println(floydwarshall.getPathMinimum(sneakyPath));
        System.out.println("Sneaky Path Maximum --------------------------");
        System.out.println(floydwarshall.getPathMaximum(sneakyPath));
        System.out.println("Sneaky Path Average --------------------------");
        System.out.println(floydwarshall.getPathAverage(sneakyPath));

        System.out.println("Graph All Pairs ------------------------------");
        floydwarshall.printMatrix(floydwarshall.generateAllPairs(floydwarshall.flowAdjustedGraph));
        System.out.println();
        System.out.print("[");

        sneakyPath = floydwarshall.generatePath(ip.startNode, ip.endNode);

        for(Integer i: sneakyPath){
            i++;
            System.out.print(i.toString() + ',');
        }
        System.out.print("]\n");

        System.out.println("Sneaky Path Minimum --------------------------");
        System.out.println(floydwarshall.getPathMinimum(sneakyPath));
        System.out.println("Sneaky Path Maximum --------------------------");
        System.out.println(floydwarshall.getPathMaximum(sneakyPath));
        System.out.println("Sneaky Path Average --------------------------");
        System.out.println(floydwarshall.getPathAverage(sneakyPath));
        System.out.println("Sneaky Path Max Graph ------------------------");
        floydwarshall.printMatrix(floydwarshall.generateSneakyPathMaxGraph());
        System.out.println("Sneaky Path Min Graph ------------------------");
        floydwarshall.printMatrix(floydwarshall.generateSneakyPathMinGraph());
        System.out.println("Sneaky Path Average Graph ------------------------");
        floydwarshall.printMatrixDouble(floydwarshall.generateSneakyPathAverageGraph());
        long endTime = System.nanoTime();
        System.out.println("Computation took " + ((endTime - startTime) / 1000000) + " milliseconds");

    }
}