package com.fourohfour;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;

/**
 * Created by brenn on 11/19/2016.
 */
public class FloydWarshall
{
    public static final Integer INFINITY = 99999;
    public Integer edgeWeightGraph[][];
    public Integer verticeCount;
    public Integer[][] nextEdge;
    public Integer[][] flowAdjustedGraph;
    public Integer[][] allPairsGraph;
    public Integer[][] sneakyPathMinGraph;
    public Integer[][] sneakyPathMaxGraph;
    public double[][] sneakyPathAverageGraph;

    public FloydWarshall(Integer verticeCount, Integer[][] edgeWeightGraph)
    {
        this.edgeWeightGraph = edgeWeightGraph;
        this.verticeCount = verticeCount;
        nextEdge = generateMatrix();
        flowAdjustedGraph = generateMatrix();
    }

    public Integer[][] floydWarshall(Integer adjacencyMatrix[][])
    {
        for (Integer i = 0; i < verticeCount; i++)
            for (Integer j = 0; j < verticeCount; j++)
                nextEdge[i][j] = j;

        for (Integer k = 0; k < verticeCount; k++)
            for (Integer i = 0; i < verticeCount; i++)
                for (Integer j = 0; j < verticeCount; j++)
                {
                    Integer combinedValue = adjacencyMatrix[i][k] + adjacencyMatrix[k][j];
                    if (combinedValue < adjacencyMatrix[i][j]){
                        adjacencyMatrix[i][j] = combinedValue;
                        nextEdge[i][j] = nextEdge[i][k];
                    }
                }

        return adjacencyMatrix;
    }

    public ArrayList<Integer> generatePath(Integer i, Integer j) {
        ArrayList<Integer> path = new ArrayList<>();
        if(nextEdge[i][j].equals(null)) {
            return path;
        }

        path.add(i);
        while(i != j){
            i = nextEdge[i][j];
            path.add(i);
        }

        return path;
    }

    public Integer getPathMinimum(ArrayList<Integer> path){
        Integer min = INFINITY;
        for(int i = 0; i < path.toArray().length-1; i++){
            if(this.flowAdjustedGraph[path.get(i).intValue()][path.get(i+1).intValue()] < min)
                min = this.flowAdjustedGraph[path.get(i).intValue()][path.get(i+1).intValue()];
        }
        return min;
    }

    public Integer getPathMaximum(ArrayList<Integer> path){
        Integer max = 0;
        for(int i = 0; i < path.toArray().length-1; i++){
            if(this.flowAdjustedGraph[path.get(i).intValue()][path.get(i+1).intValue()] > max)
                max = this.flowAdjustedGraph[path.get(i).intValue()][path.get(i+1).intValue()];
        }
        return max;
    }

    public double getPathAverage(ArrayList<Integer> path){
        Integer total = 0;
        for(int i = 0; i < path.size()-1; i++) {
            total += this.flowAdjustedGraph[path.get(i).intValue()][path.get(i + 1).intValue()];
        }
        if(path.size()-1 == 0)
            return 0;
        else
            return ((double)total)/(path.size()-1);
    }

    public Integer[][] generateSneakyPathMaxGraph() {
        sneakyPathMaxGraph = new Integer[verticeCount][verticeCount];
        for(Integer i = 0; i < verticeCount; i++){
            for(Integer j = 0; j < verticeCount; j++){
                sneakyPathMaxGraph[i][j] = getPathMaximum(generatePath(i,j));
            }
        }
        return sneakyPathMaxGraph;
    }

    public Integer[][] generateSneakyPathMinGraph() {
        sneakyPathMinGraph = new Integer[verticeCount][verticeCount];
        for(Integer i = 0; i < verticeCount; i++){
            for(Integer j = 0; j < verticeCount; j++){
                sneakyPathMinGraph[i][j] = getPathMinimum(generatePath(i,j));
            }
        }
        return sneakyPathMinGraph;
    }

    public double[][] generateSneakyPathAverageGraph() {
        sneakyPathAverageGraph = new double[verticeCount][verticeCount];
        for(Integer i = 0; i < verticeCount; i++){
            for(Integer j = 0; j < verticeCount; j++){
                sneakyPathAverageGraph[i][j] = getPathAverage(generatePath(i,j));
            }
        }
        return sneakyPathAverageGraph;
    }

    public Integer[][] generateFlowAmount(Integer[][] flowGraph) {

        for(Integer i = 0; i < verticeCount; i++) {
            for (Integer j = 0; j < verticeCount; j++) {

                Integer currentFlowAmount = flowGraph[i][j];
                ArrayList<Integer> path = generatePath(i, j);

                for (Integer x = 0; x < path.toArray().length - 1; x++) {
                    if (this.flowAdjustedGraph[path.get(x)][path.get(x + 1)] == null)
                        flowAdjustedGraph[path.get(x)][path.get(x + 1)] = currentFlowAmount;
                    else
                        flowAdjustedGraph[path.get(x)][path.get(x + 1)] += currentFlowAmount;
                }
            }
        }
        return flowAdjustedGraph;
    }

    public Integer[][] generateAllPairs(Integer[][] edgeFlowGraph) {

        for (Integer i = 0; i < verticeCount; i++) {
            for (Integer j = 0; j < verticeCount; j++) {
                    if (edgeWeightGraph[i][j].equals(INFINITY))
                        if (edgeFlowGraph[i][j] == null)
                            edgeFlowGraph[i][j] = INFINITY;
                    if (edgeFlowGraph[i][j] == null)
                        edgeFlowGraph[i][j] = 0;
                }
            }

            allPairsGraph = this.floydWarshall(edgeFlowGraph);
        return allPairsGraph;
    }


    public Integer[][] generateMatrix() {
        Integer[][] temp = new Integer[verticeCount][verticeCount];
        return temp;
    }

    public void printMatrix(Integer[][] graph) {
        for (Integer i = 1; i <= verticeCount; i++)
            System.out.print("\t" + i);

        System.out.println();
        for (Integer i = 1; i <= verticeCount; i++)
        {
            System.out.print(i + "\t");
            for (Integer j = 0; j < verticeCount; j++)

                System.out.print(graph[i-1][j] + "\t");

            System.out.println();
        }
    }
    public void printMatrixDouble(double[][] graph) {
    NumberFormat formatter = new DecimalFormat("#0.00");

    for (Integer i = 1; i <= verticeCount; i++)
        System.out.print("\t" + i);

    System.out.println();
    for (Integer i = 1; i <= verticeCount; i++)
    {
        System.out.print(i + "\t");
        for (Integer j = 0; j < verticeCount; j++)

            System.out.print(formatter.format(graph[i-1][j]) + "\t");

        System.out.println();
    }
}
}