package com.fourohfour;

import java.io.*;
import java.lang.reflect.Array;

/**
 * Created by brenn on 11/21/2016.
 */


public class InputParser {
    public String fileName;
    public Integer matrixSize;
    public Integer startNode;
    public Integer endNode;
    public Integer[][] E, F, original;
    public Integer INFINITY = 99999;

    public InputParser(String name){
        fileName = name;
    }

    public void parse() throws IOException{
        FileReader input = new FileReader(new File(fileName));
        BufferedReader bufRead = new BufferedReader(input);
        String myLine = null;
        boolean firstLine = true;
        while ( (myLine = bufRead.readLine()) != null)
        {
            String[] inputLine = myLine.split(",");
            // check to make sure you have valid data
            if(firstLine){
                matrixSize = Integer.parseInt(inputLine[0].trim());
                startNode = Integer.parseInt(inputLine[1].trim())-1;
                endNode = Integer.parseInt(inputLine[2].trim())-1;
                initializeMatrices();
                firstLine = false;
            }else if(inputLine[0].contains("E")){
                int indexOne = Integer.parseInt(inputLine[1].trim()) - 1;
                int indexTwo = Integer.parseInt(inputLine[2].trim()) - 1;
                E[indexOne][indexTwo] = Integer.parseInt(inputLine[3].trim());
                original[indexOne][indexTwo] = Integer.parseInt(inputLine[3].trim());
            }else if(inputLine[0].contains("F")){
                int indexOne = Integer.parseInt(inputLine[1].trim()) - 1;
                int indexTwo = Integer.parseInt(inputLine[2].trim()) - 1;
                F[indexOne][indexTwo] = Integer.parseInt(inputLine[3].trim());
            }
        }
    }

    public void initializeMatrices(){
        E = new Integer[matrixSize][matrixSize];
        original = new Integer[matrixSize][matrixSize];
        F = new Integer[matrixSize][matrixSize];
        for (Integer i = 0; i < matrixSize; i++) {
            for (Integer j = 0; j < matrixSize; j++) {
                if (i.equals(j)) {
                    E[i][j] = 0;
                    original[i][j] = 0;
                    F[i][j] = 0;
                } else {
                    E[i][j] = INFINITY;
                    original[i][j] = INFINITY;
                }
            }
        }
    }
}
